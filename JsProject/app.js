//const variable is most recommended in this scenario
//getElementByID() takes the string argument. 
//<select name="drinks" id="drinks"></select> Refer this for the ID from HTML
const drinksElement = document.getElementById('drinks');
const priceElement = document.getElementById('price');
const addElement = document.getElementById('add');
const cartElement = document.getElementById('cart');
const quantityElement = document.getElementById('quantity');
const payButtonElement = document.getElementById('pay');
const totalDueElement = document.getElementById('totalDue');

//used let variable - here the value may change.
//We are getting drink from a collection, fetching from API
let drinks = ['A', 'B']
let cark = []
//Default floating point value for this counting variable.
let totalDue = 0.0

//Using fetch method, get the value from backend.
fetch('https://....')
    //response as Json data.
    .then(response => response.json())
    //assign value to drinks. \
    //Drinks variable should be populated some data.
    .then(data => drinks = data)
    //append drinks elements (to add some drinks to our drinks menu)
    .then(drinks => addDrinksToMenu(drinks))

    //Using Arrow function syntax here.
const addDrinksToMenu = (drinks) => {
    //for each of the drinks, we are going to call another function
    //A simple level of abstraction
    //used lambda function - below convention to follow when it comes to lambda function
    //x - represent here the individual drink in the drinks collection.
    //For each drink ==> add drink to Menu (call addDrinkToMenu method by passing x)
    drinks.forEach(x => addDrinkToMenu(x))
    priceElement.innerText = drinks[0].price
}
//we need to do a couple of things here.
//Create an html elements that we want to append.
const addDrinkToMenu = (drink) => {
    //Creates an HTML element, passing a string (probably add ul or ol element)
    //Here instead of ul or ol element, used 'option' which is the <select>--<option>
    const drinkElement = document.createElement('option')
    //Edit some of the information we need to feed through the elements.
    //Assign a value of drink ID. (Each drink has a unique ID), this is to select a specific element
    drinkElement.value = drink.id
    //Append the child element to our HTML element
    //createTextNode == create a text string from the specified value
    drinkElement.appendChild(document.createTextNode(drink.description))
    //append DrinksElement (the drinksElements maps on the HTML select tag at Index.html)
    drinksElement.appendChild(drinkElement)    
}


//Write the function to take the value of the drinks.
const handleDrinkMenuChange = e => {
    //get the selected index.
    //Take drinks array and access specific value
    //e.target.selectedIndex ==> Select depending on which item selected from the array
    //e.target.selectedIndex (returns first element=0, second element 1 and so on.)
    const selectedDrink = drinks[e.target.selectedIndex] 
    //price is the 'id' in the HTML element -<span id='price'>  
    priceElement.innerText = selectedDrink.price 
}

const handleAddDrink = () => {
    const selectedDrink = drinks[drinksElement.selectedIndex]   
    const quantity = parseInt(quantityElement.value)
    const cartItem  = document.createElement('li')
    const lineTotal = quantity * selectedDrink.price     
    cartItem.innerText = `${selectedDrink.description} ${selectedDrink.price} ${quantity}
    ${lineTotal.toFixed(2)}`
    cartElement.appendChild(cartItem)
    totalDue += lineTotal;
    totalDueElement.innerHTML = `Total Due: ${totalDue.toFixed(2)}`
}

//Create a function to handle the Pay
const handlePay = () => {
    //prompt takes an argument
   const totalPaid = prompt("Enter the money you wish to pay: ")
   //Parse a floating point value, also possible to parse an integer if you wish
   const change = parseFloat(totalPaid) - totalDue
   //Alert the user over here
   alert(`Total change due: ${change.toFixed(2)}`)
}

drinksElement.addEventListener('change', handleDrinkMenuChange)

//This time listen to the click event
addElement.addEventListener('click', handleAddDrink)

//Event listener for handlePay
payButtonElement.addEventListener('click', handlePay)