﻿using DrinkApp.Entity;
using Microsoft.EntityFrameworkCore;
using System.Data;

namespace DrinkApp
{
    public class DrinksAppDBContext : DbContext
    {
        public DrinksAppDBContext(DbContextOptions options) : base(options)
        {
        }
        public DbSet<Drink> Drinks { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Drink>().HasData(DataSeed.GetDrinks());
            
        }
    }
}
