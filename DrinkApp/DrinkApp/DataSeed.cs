﻿using DrinkApp.Entity;

namespace DrinkApp
{
    public class DataSeed
    {
        public static List<Drink> GetDrinks()
        {
            List<Drink> drinks = new List<Drink>()
            {
                new Drink()
                {
                    Id= 1,
                    Description= "Coca Cola",
                    Price = 10.25
                },
                new Drink()
                {
                   Id= 2,
                   Description= "Pepsi",
                   Price = 9.30
                },
                new Drink()
                {
                   Id= 3,
                   Description= "Fanta",
                   Price = 8.10
                },
                new Drink()
                {
                   Id= 4,
                   Description= "Sprite",
                   Price = 12.45
                }
            };
            return drinks;
        }
    }
}
