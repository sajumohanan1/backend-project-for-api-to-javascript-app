﻿namespace DrinkApp.Entity
{
    public class Drink
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
    }
}
